#include <windows.h>
#include <shlobj.h>
#include <shellapi.h>

static IContextMenu2 *g_pICM2 = NULL;
static WNDPROC g_Original = NULL;

#ifndef NDEBUG
#define DPRINT(msg) OutputDebugString(msg)
#else
#define DPRINT(msg) (void) sizeof(msg)
#endif


LRESULT CALLBACK WinProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg) {
        case WM_INITMENUPOPUP:
        case WM_DRAWITEM:
        case WM_MENUCHAR:
        case WM_MEASUREITEM: {
            if ( g_pICM2 )
                return g_pICM2->HandleMenuMsg(uMsg, wParam, lParam);
        } break;
    }
    if (g_Original)
        return CallWindowProc(g_Original, hWnd, uMsg, wParam, lParam);
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}


ITEMIDLIST* GetPathIDL(IShellFolder* root, HWND hWnd, LPCSTR szPath)
{
    if (!root || !szPath || *szPath == 0)
        return 0;

    WCHAR szWPath[4098];
    int cc = MultiByteToWideChar(CP_UTF8, 0, szPath, -1, szWPath, 4097);
    if (cc < 1)
        return 0;
    szWPath[cc] = 0; // older windows bugfix

    WCHAR szFPath[4098];
    if (!GetFullPathNameW(szWPath, 4097, szFPath, 0))
        return 0;

    ITEMIDLIST* Rvl = 0;
    root->ParseDisplayName(hWnd, 0, szFPath, 0, &Rvl, 0);
    return Rvl;
}


IContextMenu* GetSysMenu(IShellFolder* root, HWND hWnd, ITEMIDLIST* pidl)
{
    if (!root || !pidl)
        return 0;
    
    // make pidl point to parent
    ITEMIDLIST* pos = pidl;
    for (;;)
    {
        ITEMIDLIST* next = (ITEMIDLIST*) (((BYTE*)pos) + pos->mkid.cb);
        if ( next->mkid.cb == 0 )
            break;
        pos = next;
    }
    USHORT old = pos->mkid.cb;
    pos->mkid.cb = 0;

    IShellFolder *pParent = 0;
    HRESULT hr = root->BindToObject(pidl, 0, IID_IShellFolder,
            (PVOID*) &pParent);
    pos->mkid.cb = old;

    if (FAILED(hr) || !pParent)
        return 0;

    IContextMenu *lpCM = 0;
    pParent->GetUIObjectOf(hWnd, 1, (LPCITEMIDLIST*)&pos, IID_IContextMenu,
            0, (PVOID*) &lpCM);
    pParent->Release();
    return lpCM;
}


IContextMenu* GetSysMenu(IShellFolder* root, HWND hWnd, LPCSTR szPath)
{
    IMalloc *pmlc = 0;
    if (SHGetMalloc(&pmlc) != S_OK)
        return 0;

    ITEMIDLIST* pidl = GetPathIDL(root, hWnd, szPath);
    if (!pidl)
        return 0;

    IContextMenu* Rvl = GetSysMenu(root, hWnd, pidl);
    pmlc->Free(pidl);
    pmlc->Release();
    return Rvl;
}


BOOL ShowSysContextMenu(HWND hWnd, LPCSTR szPath, int x, int y)
{
    if ( szPath == NULL || *szPath == 0 )
        return FALSE;

    IShellFolder *shRoot = 0;
    if (SHGetDesktopFolder(&shRoot) != S_OK)
    {
        DPRINT("error get root of shell");
        return FALSE;
    }
    
    IContextMenu* pContextMenu = GetSysMenu(shRoot, hWnd, szPath);
    if (!pContextMenu) {
        DPRINT("error get sysmenu!");
        shRoot->Release();
        return FALSE;
    }

    HMENU hPopup = CreatePopupMenu();
    if (!hPopup) {
        DPRINT("error create menu failed!");
        shRoot->Release();
        return FALSE;
    }

    pContextMenu->QueryContextMenu(hPopup, 0, 1, 0x7FFF, CMF_INCLUDESTATIC);
    
    pContextMenu->QueryInterface(IID_IContextMenu2, (LPVOID*)&g_pICM2);
    
    UINT nCmd = TrackPopupMenu(hPopup, TPM_RIGHTBUTTON|TPM_RETURNCMD,
            x, y, 0, hWnd, 0);
    
    if ( nCmd != 0 )
    {
        CMINVOKECOMMANDINFO ici;
        ZeroMemory(&ici, sizeof(ici));
        ici.cbSize = sizeof(ici);
        ici.hwnd = hWnd;
        ici.lpVerb = (LPCSTR)(nCmd-1);
        ici.nShow = SW_SHOWNORMAL;
        pContextMenu->InvokeCommand(&ici);
    }

    DestroyMenu(hPopup);

    if (g_pICM2)
    {
        g_pICM2->Release();
        g_pICM2 = 0;
    }

    pContextMenu->Release();
    shRoot->Release();
    return TRUE;
}


EXTERN_C HWND GetVimWindow();

LONG __declspec(dllexport) __cdecl SysMenu(LPCSTR szPath)
{
    POINT pt;
    GetCursorPos(&pt);

    HWND hWnd = CreateWindowEx(0, "STATIC", 0, WS_POPUP, 0,0,0,0,
            GetVimWindow(),0,0,0);

    g_Original = (WNDPROC) SetWindowLong(hWnd, GWL_WNDPROC, 
            (LONG) WinProc);

    ShowSysContextMenu(hWnd, szPath, pt.x, pt.y);
    DestroyWindow(hWnd);
    return 0;
}

