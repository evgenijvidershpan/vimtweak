#include <windows.h>

LONG __declspec(dllexport) __cdecl Debug(LPCSTR arg)
{
    OutputDebugString(arg);
    return 0;
}


BOOL CALLBACK EnumThreadWndProc(HWND hwnd, LPARAM lParam)
{
    char buf[4];
    if (!GetParent(hwnd) && GetClassName(hwnd, buf, 4) && !lstrcmpi("vim", buf))
    {
        *(HWND*) lParam = hwnd;
        return FALSE;
    }
    return TRUE;
}

HWND GetVimWindow()
{
    HWND hVim = NULL;
    EnumThreadWindows(GetCurrentThreadId(), EnumThreadWndProc, (LPARAM)&hVim);
    return hVim;
}

LONG __declspec(dllexport) Maximize()
{
    LONG style;
    LONG exstyle;
    HWND hText, hVim;
    RECT rc;
    int dy, width, height;

    if (!SystemParametersInfo(SPI_GETWORKAREA, 0, (PVOID)&rc, 0))
        return -1;

    hVim = GetVimWindow();
    if (!hVim)
        return -1;

    if (IsZoomed(hVim))
        SendMessage(hVim, WM_SYSCOMMAND, SC_RESTORE, 0);

    SetLastError(0);
    style = GetWindowLong(hVim, GWL_STYLE) &
        ~(WS_CAPTION|WS_BORDER|WS_THICKFRAME);
    if (GetLastError())
        return -1;
    exstyle = GetWindowLong(hVim, GWL_EXSTYLE) &
        ~WS_EX_WINDOWEDGE;
    if (GetLastError())
        return -1;

    SetWindowLong(hVim, GWL_STYLE, style);
    SetWindowLong(hVim, GWL_EXSTYLE, exstyle);

    dy = 4;
    width = rc.right - rc.left;
    height = rc.bottom + dy*2;

    SetWindowPos(hVim, 0, 0, -dy, width, height, 0);

    hText = FindWindowEx(hVim, NULL, "VimTextArea", NULL);
    if (!hText)
        return -1;

    if (!GetClientRect(hText, &rc))
        return -1;

    SetLastError(0);
    exstyle = GetWindowLong(hText, GWL_EXSTYLE) & ~WS_EX_CLIENTEDGE;
    if (GetLastError())
        return -1;

    SetWindowLong(hText, GWL_EXSTYLE, exstyle);
    SetWindowPos(hText, 0, 0, 0, rc.right - rc.left, height, SWP_NOMOVE);

    RedrawWindow(hVim, 0, 0, RDW_INVALIDATE);
    return 0;
}



LONG __declspec(dllexport) Activate()
{
    HWND hVim = GetVimWindow();
    if (!IsWindow(hVim))
        return -1;
    mouse_event(MOUSEEVENTF_MOVE,  1,  1, 0, 0);
    mouse_event(MOUSEEVENTF_MOVE, -1, -1, 0, 0);
    SetForegroundWindow(hVim);
    return 0;
}


BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD nShow, LPVOID lpr)
{
    return TRUE;
}

